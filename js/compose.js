(function ($, Drupal) {

  Drupal.behaviors.pwa_firebase_compose = {
    attach: function () {

      $(once('checkall',"#checkAll")).change(function () {
        const checkboxes = $(this).closest('details').find("input[type='checkbox']:visible");
        checkboxes.prop('checked', $(this).prop("checked"));
      });

      $(once('phonebook_search',".phonebook_search")).keyup(function () {
        var txt = $(this).val();
        const checkboxes = $(this).closest('details').find('.js-form-type-checkbox');
        checkboxes.hide();
        checkboxes.each(function () {
          if ($(this).text().toUpperCase().indexOf(txt.toUpperCase()) != -1) {
            $(this).show();
          }
        });
      });
    }
  };
}(jQuery, Drupal));
