(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.pwa_firebase = {
    attach: function (context, settings) {
      const permissionDivId = 'pwa_firebase_notifications';
      const tokenDivId = 'pwa_firebase_remove_token';
      var messaging = {};

      const $elements = $(once('pwa_firebase', 'body', context));
      $elements.each(init);

      function init(index, value) {
        if (typeof firebaseApp === "undefined") {
          const firebaseApp = firebase.initializeApp(settings.pwa_firebase.firebaseConfig);
          messaging = firebase.messaging();
          updateNotificationButtons();
          messaging.onMessage(function (payload) {
            const title = payload.notification.title;
            const options = {
              body: payload.notification.body,
              icon: payload.notification.icon,
            };
            new Notification(title, options);
          });
        }
        bindNotificationButtons();
      }
      function updateNotificationButtons() {
        const isTokenSent = isTokenSentToServer();
        if (isTokenSent) {
          $(`.notification_manager #${tokenDivId}`).show();
          $(`.notification_manager #${permissionDivId}`).hide();
        } else {
          let isShowPopup = sessionStorage.getItem('pwa_firebase_popup');
          if (!JSON.parse(isShowPopup)) {
            showPopup();
          }
          $(`.notification_manager #${permissionDivId}`).show();
          $(`.notification_manager #${tokenDivId}`).hide();
        }
      }

      function bindNotificationButtons() {
        // Enable notification from block
        $(".notification_manager #" + permissionDivId).on("click", function () {
          requestPermission();
        });
        // Remove token from block.
        $(".notification_manager #" + tokenDivId).on("click", function () {
          deleteToken();
        });
      }

      function showPopup() {
        const dialog = `<div class="text-center dialog">${Drupal.t("This application will send notifications to your device")}
          <div class="ml-2 d-flex align-items-center justify-content-center g-2">
            <button class="allow-button btn btn-success mr-1" id="${permissionDivId}">${Drupal.t('Allow')}</button>
          </div></div>`;
        const notify = $.notify(dialog, { allow_dismiss: true, delay: 5000 });

        $(`.dialog #${permissionDivId}`).on("click", function () {
          notify.close();
          requestPermission();
        });
        sessionStorage.setItem('pwa_firebase_popup', true);
        setTimeout(() => notify.close(), 5000);
      }

      // Send the registration token your application server, so that it can:
      // - send messages back to this app
      // - subscribe/unsubscribe the token from topics
      function sendTokenToServer(currentToken) {
        if (!isTokenSentToServer()) {
          $.post(settings.pwa_firebase.sendToken, { token: currentToken }, function (data) {
            if (!data) {
              console.info('Token sent to the server');
            }
          });
          sessionStorage.setItem('pwa_firebase_popup', false);
          setTokenSentToServer(true);
          $('#' + permissionDivId).hide();
          //console.log('Token already sent to server so won\'t send it again unless it changes');
        }
      }

      function isTokenSentToServer() {
        return window.localStorage.getItem('sentToServer') === 'true';
      }

      function setTokenSentToServer(sent) {
        window.localStorage.setItem('sentToServer', sent.toString());
        updateNotificationButtons();
      }

      function registerServiceWorker() {
        if ('serviceWorker' in navigator) {
          window.addEventListener('load', function() {
            navigator.serviceWorker.register(settings.pwa_firebase.serviceworker)
              .then(function(registration) {
                console.log('Service Worker registered with scope:', registration.scope);
              })
              .catch(function(error) {
                console.error('Service Worker registration failed:', error);
              });
          });
        }
      }

      function requestPermission() {
        registerServiceWorker();
        Notification.requestPermission().then((permission) => {
          if (permission === 'granted') {
            messaging.getToken({ vapidKey: settings.pwa_firebase.VapidKey }).then((currentToken) => {
              if (currentToken) {
                sendTokenToServer(currentToken);
              }
            }).catch((err) => {
              console.log('Error retrieving registration token. ', err);
            });
          } else {
            // Permission denied
          }
        });
      }

      function deleteToken() {
        messaging.getToken({ vapidKey: settings.pwa_firebase.VapidKey }).then((currentToken) => {
          $.post(settings.pwa_firebase.sendToken, { token: currentToken, action: 'delete' }, function (data) {
            messaging.deleteToken(currentToken).then(() => {
              console.log('Token deleted.');
              setTokenSentToServer(false);
            }).catch((err) => {
              console.log('Unable to delete token. ', err);
            });
          });
        }).catch((err) => {
          console.log('Error retrieving registration token. ', err);
        });
      }

    }
  };
})(jQuery, Drupal, drupalSettings, once);
