// Connect when we have event 'install'
self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open('my-site').then(function (cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/firebase-messaging-sw.js',
        '/themes/contrib/bootstrap5_admin/logo.svg',
        '/themes/contrib/bootstrap5_admin/css/styles.css',
      ]);
    })
  );
});

// Connect when we have event 'fetch'
self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      return response || fetch(event.request);
    })
  );
});

// Connect when we have event 'activate'
self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.filter(function (cacheName) {
          return cacheName.startsWith('my-site') && cacheName !== 'my-site';
        }).map(function (cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});
