/*
* Project: Bootstrap Notify = v3.1.5
* Description: Turns standard Bootstrap alerts into "Growl-like" notifications.
* Author: Mouse0270 aka Robert McIntosh
* License: MIT License
* Website: https://github.com/mouse0270/bootstrap-growl
*/

/* global define:false, require: false, jQuery:false */

(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node/CommonJS
    factory(require('jquery'));
  } else {
    // Browser globals
    factory(jQuery);
  }
}(function ($) {
  // Create the defaults once
  let defaults = {
    element: 'body',
    position: null,
    type: "info",
    allow_dismiss: true,
    allow_duplicates: true,
    newest_on_top: false,
    showProgressbar: false,
    placement: {
      from: "top",
      align: "right"
    },
    offset: 20,
    spacing: 10,
    z_index: 1031,
    delay: 5000,
    timer: 1000,
    url_target: '_blank',
    mouse_over: null,
    animate: {
      enter: 'animated fadeInDown',
      exit: 'animated fadeOutUp'
    },
    onShow: null,
    onShown: null,
    onClose: null,
    onClosed: null,
    onClick: null,
    icon_type: 'class',
    template: '<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button><span data-notify="icon"></span> <span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>'
  };

  String.format = function (str, ...args) {
    return str.replace(/{{(\d+)}|\{(\d+)\}/g, function (match, doubleBraceIndex, singleBraceIndex) {
      if (doubleBraceIndex !== undefined) {
        return match;
      }
      let num = parseInt(singleBraceIndex);
      return args[num];
    });
  };

  function isDuplicateNotification(notification) {
    const title = $("<div>").html(notification.settings.content.title).text().trim();
    const message = $("<div>").html(notification.settings.content.message).text().trim();
    const typeClass = 'alert-' + notification.settings.type;

    const isDupe = $('[data-notify="container"]').toArray().some(el => {
      const $el = $(el);
      const $title = $el.find('[data-notify="title"]');
      const $message = $el.find('[data-notify="message"]');
      const $type = $el.hasClass(typeClass);

      return (
        $title.length > 0 &&
        $message.length > 0 &&
        $title.text().trim() === title &&
        $message.text().trim() === message &&
        $type
      );
    });
    return isDupe;
  }

  function Notify(element, content, options) {
    // Setup Content of Notify
    const contentObj = {
      content: {
        message: typeof content === 'object' ? content.message : content,
        title: content.title ? content.title : '',
        icon: content.icon ? content.icon : '',
        url: content.url ? content.url : '#',
        target: content.target ? content.target : '-'
      }
    };

    options = $.extend(true, {}, contentObj, options);
    this.settings = $.extend(true, {}, defaults, options);
    this._defaults = defaults;
    if (this.settings.content.target === "-") {
      this.settings.content.target = this.settings.url_target;
    }
    this.animations = {
      start: 'webkitAnimationStart oanimationstart MSAnimationStart animationstart',
      end: 'webkitAnimationEnd oanimationend MSAnimationEnd animationend'
    };

    if (typeof this.settings.offset === 'number') {
      this.settings.offset = {
        x: this.settings.offset,
        y: this.settings.offset
      };
    }

    //if duplicate messages are not allowed, then only continue if this new message is not a duplicate of one that it already showing
    if (this.settings.allow_duplicates || (!this.settings.allow_duplicates && !isDuplicateNotification(this))) {
      this.init();
    }
  }

  $.extend(Notify.prototype, {
    init: function () {
      var self = this;

      this.buildNotify();
      if (this.settings.content.icon) {
        this.setIcon();
      }
      if (this.settings.content.url != "#") {
        this.styleURL();
      }
      this.styleDismiss();
      this.placement();
      this.bind();

      this.notify = {
        $ele: this.$ele,
        update(command, update) {
          const { $ele } = this;
          const { type, content, spacing, offset, delay } = self.settings;
          const $progressbar = $ele.find('[data-notify="progressbar"] > .progress-bar');

          const commands = typeof command === "string" ? { [command]: update } : command;

          for (const cmd in commands) {
            switch (cmd) {
              case "type":
                $ele.removeClass(`alert-${type}`);
                $progressbar.removeClass(`progress-bar-${type}`);
                self.settings.type = commands[cmd];
                $ele.addClass(`alert-${commands[cmd]}`);
                $progressbar.addClass(`progress-bar-${commands[cmd]}`);
                break;

              case "icon":
                const $icon = $ele.find('[data-notify="icon"]');
                if (self.settings.icon_type.toLowerCase() === 'class') {
                  $icon.removeClass(content.icon).addClass(commands[cmd]);
                } else {
                  if (!$icon.is('img')) {
                    $icon.find('img');
                  }
                  $icon.attr('src', commands[cmd]);
                }
                content.icon = commands[command];
                break;

              case "progress":
                const newDelay = delay - (delay * (commands[cmd] / 100));
                $ele.data('notify-delay', newDelay);
                $progressbar.attr('aria-valuenow', commands[cmd]).css('width', `${commands[cmd]}%`);
                break;

              case "url":
                $ele.find('[data-notify="url"]').attr('href', commands[cmd]);
                break;

              case "target":
                $ele.find('[data-notify="url"]').attr('target', commands[cmd]);
                break;

              default:
                $ele.find(`[data-notify="${cmd}"]`).html(commands[cmd]);
            }
          }
          const posX = $ele.outerHeight() + parseInt(spacing) + parseInt(offset.y);
          self.reposition(posX);
        },
        close() {
          self.close();
        }
      };

    },
    buildNotify() {
      const { content, template, type, placement, allow_dismiss, delay, showProgressbar } = this.settings;
      this.$ele = $(String.format(template, type, content.title, content.message, content.url, content.target));

      const $dismiss = this.$ele.find('[data-notify="dismiss"]');
      const $progressbar = this.$ele.find('[data-notify="progressbar"]');

      this.$ele.attr('data-notify-position', `${placement.from}-${placement.align}`);

      if (!allow_dismiss) {
        $dismiss.hide();
      }

      if (delay <= 0 && !showProgressbar || !showProgressbar) {
        $progressbar.remove();
      }
    },
    setIcon() {
      const $icon = this.$ele.find('[data-notify="icon"]');
      const { icon_type, content } = this.settings;

      if (icon_type.toLowerCase() === 'class') {
        $icon.addClass(content.icon);
      } else {
        if ($icon.is('img')) {
          $icon.attr('src', content.icon);
        } else {
          $icon.append(`<img src="${content.icon}" alt="Notify Icon" />`);
        }
      }
    },
    styleDismiss: function () {
      this.$ele.find('[data-notify="dismiss"]').css({
        position: 'absolute',
        right: '10px',
        top: '5px',
        zIndex: this.settings.z_index + 2
      });
    },
    styleURL: function () {
      this.$ele.find('[data-notify="url"]').css({
        backgroundImage: 'url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)',
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%',
        zIndex: this.settings.z_index + 1
      });
    },
    placement: function () {
      const self = this;
      let offsetAmt = this.settings.offset.y;
      const css = {
        display: 'inline-block',
        margin: '0px auto',
        position: this.settings.position ? this.settings.position : (this.settings.element === 'body' ? 'fixed' : 'absolute'),
        transition: 'all .5s ease-in-out',
        zIndex: this.settings.z_index
      };
      let hasAnimation = false;
      const settings = this.settings;

      $(`[data-notify-position="${this.settings.placement.from}-${this.settings.placement.align}"]:not([data-closing="true"])`).each(function () {
        offsetAmt = Math.max(offsetAmt, parseInt($(this).css(settings.placement.from)) + parseInt($(this).outerHeight()) + parseInt(settings.spacing));
      });

      if (this.settings.newest_on_top === true) {
        offsetAmt = this.settings.offset.y;
      }
      css[this.settings.placement.from] = `${offsetAmt}px`;

      switch (this.settings.placement.align) {
        case "left":
        case "right":
          css[this.settings.placement.align] = `${this.settings.offset.x}px`;
          break;

        case "center":
          css.left = 0;
          css.right = 0;
          break;
      }

      this.$ele.css(css).addClass(this.settings.animate.enter);

      $.each(Array('webkit-', 'moz-', 'o-', 'ms-', ''), function (index, prefix) {
        self.$ele[0].style[`${prefix}AnimationIterationCount`] = 1;
      });

      $(this.settings.element).append(this.$ele);

      if (this.settings.newest_on_top === true) {
        offsetAmt = (parseInt(offsetAmt) + parseInt(this.settings.spacing)) + this.$ele.outerHeight();
        this.reposition(offsetAmt);
      }

      if (typeof self.settings.onShow === 'function') {
        self.settings.onShow.call(this.$ele);
      }

      this.$ele.one(this.animations.start, function () {
        hasAnimation = true;
      }).one(this.animations.end, function () {
        self.$ele.removeClass(self.settings.animate.enter);
        if (typeof self.settings.onShown === 'function') {
          self.settings.onShown.call(this);
        }
      });

      setTimeout(function () {
        if (!hasAnimation) {
          if (typeof self.settings.onShown === 'function') {
            self.settings.onShown.call(this);
          }
        }
      }, 600);
    },
    bind() {
      const self = this;

      this.$ele.find('[data-notify="dismiss"]').on('click', function () {
        self.close();
      });

      if (typeof self.settings.onClick === 'function') {
        this.$ele.on('click', '[data-notify="dismiss"]', function (event) {
          if (event.target !== self.$ele.find('[data-notify="dismiss"]')[0]) {
            self.settings.onClick.call(this, event);
          }
        });
      }

      this.$ele.on('mouseover', function () {
        $(this).data('data-hover', true);
      }).on('mouseout', function () {
        $(this).data('data-hover', false);
      }).data('data-hover', false);

      if (this.settings.delay > 0) {
        self.$ele.data('notify-delay', self.settings.delay);
        const timer = setInterval(function () {
          const delay = parseInt(self.$ele.data('notify-delay')) - self.settings.timer;
          if ((self.$ele.data('data-hover') === false && self.settings.mouse_over === "pause") || self.settings.mouse_over !== "pause") {
            const percent = ((self.settings.delay - delay) / self.settings.delay) * 100;
            self.$ele.data('notify-delay', delay);
            self.$ele.find('[data-notify="progressbar"] > div').attr('aria-valuenow', percent).css('width', percent + '%');
          }
          if (delay <= -(self.settings.timer)) {
            clearInterval(timer);
            self.close();
          }
        }, self.settings.timer);
      }
    },
    close() {
      const self = this;
      const posX = parseInt(this.$ele.css(this.settings.placement.from));
      let hasAnimation = false;

      this.$ele.attr('data-closing', 'true').addClass(this.settings.animate.exit);
      self.reposition(posX);

      if (typeof self.settings.onClose === 'function') {
        self.settings.onClose.call(this.$ele);
      }

      this.$ele.one(this.animations.start, function () {
        hasAnimation = true;
      }).one(this.animations.end, function () {
        $(this).remove();
        if (typeof self.settings.onClosed === 'function') {
          self.settings.onClosed.call(this);
        }
      });

      setTimeout(function () {
        if (!hasAnimation) {
          self.$ele.remove();
          if (typeof self.settings.onClosed === 'function') {
            self.settings.onClosed.call(this);
          }
        }
      }, 600);
    },
    reposition(posX) {
      const self = this;
      const notifies = `[data-notify-position="${this.settings.placement.from}-${this.settings.placement.align}"]:not([data-closing="true"])`;
      let $elements = this.$ele.nextAll(notifies);
      if (this.settings.newest_on_top === true) {
        $elements = this.$ele.prevAll(notifies);
      }
      $elements.each(function () {
        $(this).css(self.settings.placement.from, posX);
        posX = (parseInt(posX) + parseInt(self.settings.spacing)) + $(this).outerHeight();
      });
    }
  });

  $.notify = function (content, options) {
    let plugin = new Notify(this, content, options);
    return plugin.notify;
  };
  $.notifyDefaults = function (options) {
    defaults = $.extend(true, {}, defaults, options);
    return defaults;
  };

  $.notifyClose = function (selector) {

    if (typeof selector === "undefined" || selector === "all") {
      $('[data-notify]').find('[data-notify="dismiss"]').trigger('click');
    }else if(selector === 'success' || selector === 'info' || selector === 'warning' || selector === 'danger'){
      $('.alert-' + selector + '[data-notify]').find('[data-notify="dismiss"]').trigger('click');
    } else if(selector){
      $(selector + '[data-notify]').find('[data-notify="dismiss"]').trigger('click');
    }
    else {
      $('[data-notify-position="' + selector + '"]').find('[data-notify="dismiss"]').trigger('click');
    }
  };

  $.notifyCloseExcept = function (selector) {

    if(selector === 'success' || selector === 'info' || selector === 'warning' || selector === 'danger'){
      $('[data-notify]').not('.alert-' + selector).find('[data-notify="dismiss"]').trigger('click');
    } else{
      $('[data-notify]').not(selector).find('[data-notify="dismiss"]').trigger('click');
    }
  };
}));
