<?php

namespace Drupal\pwa_firebase\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block for user disable notification.
 */
#[Block(
  id: "notification_manager",
  admin_label: new TranslatableMarkup("Notification permission"),
  category: new TranslatableMarkup("User")
)]
class NotificationManagerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a SyndicateBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('pwa_firebase.settings');
    $id = 'message';
    if (!empty($config->get('firebase_message_id'))) {
      $id = str_replace('#', '', $config->get('firebase_message_id'));
    }
    $firebase_config = [
      'firebaseConfig' => [
        'apiKey' => trim($config->get('firebase_apiKey_id')),
        'authDomain' => trim($config->get('firebase_project_id')) . '.firebaseapp.com',
        'projectId' => trim($config->get('firebase_project_id')),
        'storageBucket' => trim($config->get('firebase_project_id')) . '.appspot.com',
        'databaseURL' => 'https://' . trim($config->get('firebase_project_id')) . '.firebaseio.com',
        'messagingSenderId' => trim($config->get('firebase_sender_id')),
        'appId' => trim($config->get('firebase_app_id')),
      ],
      'VapidKey' => trim($config->get('firebase_vap_id')),
      'idmessage' => trim($config->get('firebase_message_id')),
      'sendToken' => Url::fromRoute('pwa_firebase.send_token')->toString(),
      'serviceworker' => Url::fromRoute('pwa_firebase.serviceworker')->toString(),
    ];
    if (!empty($config->get('firebase_measurement_id'))) {
      $firebase_config['measurementId'] = trim($config->get('firebase_measurement_id'));
    }
    return [
      '#theme' => 'firebase_notification',
      '#attributes' => [
        'id' => $id,
        'class' => ['notification_manager', 'btn-group'],
      ],
      '#attached' => [
        'library' => [
          'pwa_firebase/pwa_firebase',
        ],
        'drupalSettings' => [
          'pwa_firebase' => $firebase_config,
        ],
      ],
    ];
  }

}
