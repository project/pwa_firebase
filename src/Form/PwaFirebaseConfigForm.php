<?php

namespace Drupal\pwa_firebase\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form firebase.
 */
class PwaFirebaseConfigForm extends ConfigFormBase {

  /**
   * Name of the config.
   *
   * @var string
   */
  public static $configName = 'pwa_firebase.settings';

  /**
   * PwaFirebaseConfigForm constructor.
   *
   * @param \Drupal\Core\Extension\ExtensionPathResolver $extensionPathResolver
   *   The extension path resolver service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(public ExtensionPathResolver $extensionPathResolver, public EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * Creates an instance of PwaFirebaseConfigForm.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container interface.
   *
   * @return static
   *   The created instance of PwaFirebaseConfigForm.
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('extension.path.resolver'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::$configName];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'pwa_firebase_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::$configName);
    $values = $form_state->getUserInput();
    $projectId = $config->get('firebase_project_id') ?? '[your-project-id]';
    if (!empty($values['firebase_project_id'])) {
      $projectId = $values['firebase_project_id'];
    }
    $general = "https://console.firebase.google.com/u/0/project/$projectId/settings/general";
    $endPoint = "https://fcm.googleapis.com/v1/projects/$projectId/messages:send";

    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General firebase configuration'),
      '#description' => $this->t('Go to <a href="@href">@href</a>', ['@href' => $general]),
      '#open' => TRUE,
    ];
    $form['general']['firebase_project_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase project id'),
      '#description' => $this->t('Google Firebase: Project Settings → Setting → Project ID'),
      '#default_value' => $projectId,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::projectIdAjaxCallback',
        'event' => 'change',
        'wrapper' => 'pwa-firebase-settings-form',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];
    $form['general']['firebase_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase endpoint'),
      '#description' => $this->t('Google Firebase Cloud Messaging endpoint. %endpoint', ['%endpoint' => $endPoint]),
      '#default_value' => str_replace('[project-id]', $projectId, $config->get('firebase_endpoint') ?? $endPoint),
      '#required' => TRUE,
    ];
    $form['credentials_legacy'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Credentials - Service account key'),
      '#description' => $this->t("Firebase Console → [@project] → Service accounts → Generate new private key. <a href='@key'>@key</a>", [
        '@project' => $projectId,
        '@key' => "https://console.firebase.google.com/project/$projectId/settings/serviceaccounts/adminsdk",
      ]),
      '#upload_location' => 'public://firebase/credentials/',
      '#default_value' => $config->get('credentials_legacy') ?? NULL,
      '#upload_validators' => ['file_validate_extensions' => ['json']],
    ];
    $form['cloud_messageing'] = [
      '#type' => 'details',
      '#title' => $this->t('Firebase - Cloud Messaging'),
      '#description' => $this->t("<a href='@key'>@key</a>", ['@key' => "https://console.firebase.google.com/u/0/project/$projectId/settings/cloudmessaging"]),
    ];
    $form['cloud_messageing']['firebase_sender_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase sender id'),
      '#description' => $this->t('Sender id: Project Settings → Cloud Messaging → Project credentials → Sender ID'),
      '#default_value' => $config->get('firebase_sender_id'),
      '#required' => TRUE,
    ];
    $form['cloud_messageing']['firebase_vap_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Web Push certificates'),
      '#rows' => 2,
      '#description' => $this->t('Web Push certificates: Project Settings → Cloud Messaging → Web configuration → Key pair'),
      '#default_value' => $config->get('firebase_vap_id'),
    ];

    $form['your_apps'] = [
      '#type' => 'details',
      '#title' => $this->t('Your apps - Web apps Information'),
      '#description' => $this->t('<a href="@key">@key</a>', ['@key' => "https://console.firebase.google.com/u/0/project/$projectId/settings/general/web"]),
    ];
    $form['your_apps']['firebase_apiKey_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase API Key'),
      '#description' => $this->t('Your apps: Project Settings → General → Web API Key'),
      '#default_value' => $config->get('firebase_apiKey_id'),
    ];
    $form['your_apps']['firebase_app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase app Key'),
      '#description' => $this->t('Your apps: Project Settings → General → Your apps / App ID'),
      '#default_value' => $config->get('firebase_app_id'),
    ];

    $form['your_apps']['firebase_measurement_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase analytics'),
      '#description' => $this->t('If you want to integrate google analytic. Google Firebase Project Settings → General → Your apps → measurementId'),
      '#default_value' => $config->get('firebase_measurement_id'),
    ];
    $form['your_apps']['firebase_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Firebase version'),
      '#description' => $this->t('You should use the stable version x.xx.0'),
      '#default_value' => $config->get('firebase_version'),
    ];

    $form['manifest'] = [
      '#type' => 'details',
      '#title' => $this->t('Override manifest.json'),
      '#description' => $this->t("It will configure to file <a href='/manifest.json'>manifest.json</a>."),
      '#open' => FALSE,
    ];
    $form['manifest']['#description'] .= $this->t('For icons, native support with svg logo. If you want a another type of image, go to <a href="https://www.pwabuilder.com/imageGenerator">Image Generator</a>. <b>Extract file icons.json and all folder icons to your root theme</b> it will add to manifest');
    $form['manifest']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Web app name'),
      '#description' => $this->t("The name for the application that needs to be displayed to the user."),
      '#default_value' => $config->get('name'),
      "#maxlength" => 55,
    ];

    $form['manifest']['short_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Short name'),
      '#description' => $this->t("A short application name, this one gets displayed on the user's homescreen."),
      '#default_value' => $config->get('short_name'),
      '#maxlength' => 25,
    ];

    $form['manifest']['lang'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Language'),
      '#description' => $this->t('The default language of the manifest.'),
      '#default_value' => $config->get('lang'),
      '#maxlength' => 25,
    ];

    $form['manifest']['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('The description of your PWA.'),
      '#default_value' => $config->get('description'),
      '#maxlength' => 255,
    ];

    $form['manifest']['start_url'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Start URL'),
      '#description' => $this->t('Start URL.'),
      '#default_value' => $config->get('start_url'),
      '#rows' => 1,
    ];

    $form['manifest']['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope'),
      '#description' => $this->t('Restricts what web pages can be viewed while the manifest is applied. If the user navigates outside the scope, it reverts to a normal web page inside a browser tab or window.'),
      '#default_value' => $config->get('scope'),
      '#maxlength' => 255,
    ];

    $form['manifest']['orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation'),
      '#description' => $this->t('The default screen orientation for all top-level browsing contexts of the web application.'),
      '#options' => [
        'any' => $this->t('Any'),
        'natural' => $this->t('Natural'),
        'landscape' => $this->t('Landscape'),
        'landscape-primary' => $this->t('Landscape primary'),
        'landscape-secondary' => $this->t('Landscape secondary'),
        'portrait' => $this->t('Portrait'),
        'portrait-primary' => $this->t('Portrait primary'),
        'portrait-secondary' => $this->t('Portrait secondary'),
      ],
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $config->get('orientation'),
    ];
    $form['manifest']['categories'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Categories'),
      '#description' => $this->t('The default is an optional member that specifies an array of categories that the application belongs to.'),
      '#options' => [
        'business' => $this->t('Business'),
        'books' => $this->t('Books'),
        'education' => $this->t('Education'),
        'entertainment' => $this->t('Entertainment'),
        'finance' => $this->t('Finance'),
        'fitness' => $this->t('Fitness'),
        'food' => $this->t('Food'),
        'games' => $this->t('Games'),
        'government' => $this->t('Government'),
        'health' => $this->t('Health'),
        'kids' => $this->t('Kids'),
        'lifestyle' => $this->t('Lifestyle'),
        'magazines' => $this->t('Magazines'),
        'medical' => $this->t('Medical'),
        'music' => $this->t('Music'),
        'navigation' => $this->t('Navigation'),
        'news' => $this->t('News'),
        'personalization' => $this->t('Personalization'),
        'photo' => $this->t('Photo'),
        'politics' => $this->t('Politics'),
        'productivity' => $this->t('Productivity'),
        'security' => $this->t('Security'),
        'shopping' => $this->t('Shopping'),
        'social' => $this->t('Social'),
        'sports' => $this->t('Sports'),
        'travel' => $this->t('Travel'),
        'utilities' => $this->t('Utilities'),
        'weather' => $this->t('Weather'),
      ],
      '#default_value' => $config->get('categories'),
    ];

    $form['manifest']['theme_color'] = [
      "#type" => 'color',
      "#title" => $this->t('Theme color'),
      "#description" => $this->t('This color sometimes affects how the application is displayed by the OS.'),
      '#default_value' => $config->get('theme_color'),
    ];

    $form['manifest']['background_color'] = [
      "#type" => 'color',
      "#title" => $this->t('Background color'),
      "#description" => $this->t('This color gets shown as the background when the application is launched'),
      '#default_value' => $config->get('background_color'),
    ];

    $form['manifest']['display'] = [
      "#type" => 'select',
      "#title" => $this->t('Display type'),
      "#description" => $this->t('This determines which UI elements from the OS are displayed.'),
      "#options" => [
        'fullscreen' => $this->t('Fullscreen'),
        'standalone' => $this->t('Standalone'),
        'minimal-ui' => $this->t('Minimal ui'),
        'browser' => $this->t('Browser'),
      ],
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => $config->get('display'),
    ];

    $form['manifest']['display_override'] = [
      "#type" => 'checkboxes',
      "#title" => $this->t('Display override'),
      "#description" => $this->t('Used to determine the preferred display mode.'),
      "#options" => [
        'fullscreen' => $this->t('Fullscreen'),
        'standalone' => $this->t('Standalone'),
        'minimal-ui' => $this->t('Minimal ui'),
        'browser' => $this->t('Browser'),
        'window-controls-overlay' => $this->t('Window controls overlay'),
      ],
      '#default_value' => $config->get('display_override'),
    ];

    $form['sw'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom service worker'),
      "#description" => $this->t('Add custom code to firebase-messaging-sw.js.'),
      '#open' => FALSE,
    ];

    $modulePath = $this->extensionPathResolver->getPath('module', 'pwa_firebase');
    $sw_example = '/' . $modulePath . '/js/service_worker_example.js';
    $form['sw']['service_worker'] = [
      "#type" => 'textarea',
      '#title' => $this->t('Service worker custom'),
      '#description' => $this->t("It will add to file <a href='/firebase-messaging-sw.js'>firebase-messaging-sw.js</a>. See example <a href='@sw_example'>service_worker_example.js</a>", ['@sw_example' => $sw_example]),
      '#default_value' => $config->get('service_worker'),
    ];

    $form['firebase_enable_authorized'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use only with authorized user'),
      '#description' => $this->t('Checked which project is not public, it is active when user has logged in'),
      '#default_value' => $config->get('firebase_enable_authorized'),
    ];

    $form['firebase_message_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom selector of div that display notification'),
      '#description' => $this->t('You can add &lt;div id="message"> in your site'),
      '#default_value' => $config->get('firebase_message_id'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function projectIdAjaxCallback(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $credentials_legacy = $form_state->getValue('credentials_legacy');
    if (!empty($credentials_legacy)) {
      $fid = current($credentials_legacy);
      $file = $this->entityTypeManager->getStorage('file')->load($fid);
      if (!$file->status->value) {
        $file->set('status', TRUE)->save();
      }
    }
    $this->config(self::$configName)
      ->set('firebase_endpoint', $form_state->getValue('firebase_endpoint'))
      ->set('credentials_legacy', $form_state->getValue('credentials_legacy'))
      ->set('firebase_sender_id', $form_state->getValue('firebase_sender_id'))
      ->set('firebase_project_id', $form_state->getValue('firebase_project_id'))
      ->set('firebase_apiKey_id', $form_state->getValue('firebase_apiKey_id'))
      ->set('firebase_app_id', $form_state->getValue('firebase_app_id'))
      ->set('firebase_vap_id', $form_state->getValue('firebase_vap_id'))
      ->set('firebase_measurement_id', $form_state->getValue('firebase_measurement_id'))
      ->set('firebase_version', $form_state->getValue('firebase_version'))
      ->set('firebase_enable_authorized', $form_state->getValue('firebase_enable_authorized'))
      ->set('firebase_message_id', $form_state->getValue('firebase_message_id'))
      ->set('name', $form_state->getValue('name'))
      ->set('short_name', $form_state->getValue('short_name'))
      ->set('lang', $form_state->getValue('lang'))
      ->set('description', $form_state->getValue('description'))
      ->set('orientation', $form_state->getValue('orientation'))
      ->set('start_url', $form_state->getValue('start_url'))
      ->set('scope', $form_state->getValue('scope'))
      ->set('theme_color', $form_state->getValue('theme_color'))
      ->set('background_color', $form_state->getValue('background_color'))
      ->set('display', $form_state->getValue('display'))
      ->set('display_override', $form_state->getValue('display_override'))
      ->set('categories', $form_state->getValue('categories'))
      ->set('service_worker', $form_state->getValue('service_worker'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

}
