<?php

namespace Drupal\pwa_firebase;

/**
 * Provides an interface defining a firebase.
 */
interface FirebaseInterface {

  /**
   * Function to send a notification to all the users.
   */
  public function sendMessageToAllUsers($title = '', $message = '', $url = NULL, $option = []);

  /**
   * Function to send a notification to 1 user.
   */
  public function sendMessageToUser($uid, $title, $message, $url = NULL, $option = []);

  /**
   * Function send message to firebase.
   */
  public function sendNotification($token, $title, $message, $url = NULL, $option = []);

}
