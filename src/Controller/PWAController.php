<?php

namespace Drupal\pwa_firebase\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller routines for help routes.
 */
class PWAController extends ControllerBase {

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request parameter.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $fileUrlGenerator
   *   The file URL generator.
   */
  public function __construct(protected Connection $database, protected Request $request, protected FileUrlGeneratorInterface $fileUrlGenerator) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('file_url_generator'),
    );
  }

  /**
   * Function to generate manifest.json.
   */
  public function manifest() {
    $config = $this->config('pwa_firebase.settings');
    $logo = theme_get_setting('logo.url');
    // Support file svg.
    $type = 'image/svg+xml';
    $size = 'any';
    $icons = [
      [
        'src' => $logo,
        'sizes' => $size,
        'type' => $type,
        'purpose' => 'any',
      ],
    ];
    $path_parts = pathinfo($logo);
    $ext = strtolower($path_parts['extension']);
    if ($ext != 'svg') {
      // Check file icons.json in theme.
      $iconsFile = DRUPAL_ROOT . $path_parts['dirname'] . '/icons.json';
      if (file_exists($iconsFile)) {
        $jsonString = file_get_contents($iconsFile);
        $icons = current(json_decode($jsonString, TRUE));
        foreach ($icons as &$icon) {
          $icon['src'] = $path_parts["dirname"] . '/' . $icon['src'];
        }
      }
      else {
        $typeId = exif_imagetype(DRUPAL_ROOT . $logo);
        $type = image_type_to_mime_type($typeId);
        $image_size = getimagesize(DRUPAL_ROOT . $logo);
        $size = $image_size[0] . 'x' . $image_size[1];
        $icons = [
          [
            'src' => $logo,
            'sizes' => $size,
            'type' => $type,
            'purpose' => 'any maskable',
          ],
        ];
      }
    }
    else {
      // Include at least one square icon svg.
      $icons[] = [
        'src' => $logo,
        'sizes' => '192x192',
        'type' => $type,
        'purpose' => 'maskable',
      ];
    }
    $lang = $this->languageManager()->getDefaultLanguage()->getId();
    if (!empty($config->get('lang'))) {
      $lang = $config->get('lang');
    }
    $theme_color = $config->get('theme_color') ?? '#000000';
    $bg_color = $config->get('background_color') ?? '#ffffff';
    $systemSite = $this->config('system.site');
    $manifest = [
      'name' => $systemSite->get('name'),
      'icons' => $icons,
      'start_url' => !empty($config->get('start_url')) ? $config->get('start_url') : '/',
      'scope' => !empty($config->get('scope')) ? $config->get('scope') : '/',
      'display' => !empty($config->get('display')) ? $config->get('display') : 'standalone',
      'lang' => $lang,
      'gcm_sender_id' => $config->get('firebase_sender_id'),
      'description' => !empty($systemSite->get('slogan')) ? $systemSite->get('slogan') : $systemSite->get('name'),
      'dir' => 'auto',
      'orientation' => 'any',
      'theme_color' => '#' . str_replace('#', '', $theme_color),
      'background_color' => '#' . str_replace('#', '', $bg_color),
    ];
    $short_name = $config->get('short_name') ?? $systemSite->get('slogan');
    if (!empty($short_name)) {
      $manifest['short_name'] = $short_name;
    }
    if (!empty($manifest["short_name"])) {
      $manifest['short_name'] = current(explode(' ', $systemSite->get('name')));
    }
    $screenshots = DRUPAL_ROOT . $path_parts['dirname'] . '/screenshot.png';
    if (file_exists($screenshots)) {
      $image_size = getimagesize($screenshots);
      $manifest['screenshots'] = [
        [
          'src' => $path_parts['dirname'] . '/screenshot.png',
          'sizes' => $image_size[0] . 'x' . $image_size[1],
          'type' => 'image/png',
          'platform' => 'wide',
        ],
      ];
    }
    // Override manifest.
    $override = [
      'name',
      'short_name',
      'lang',
      'description',
      'orientation',
      'start_url',
      'scope',
      'theme_color',
      'background_color',
      'display',
      'display_override',
      'categories',
    ];
    foreach ($override as $cnf) {
      if (!empty($config->get($cnf))) {
        $manifest[$cnf] = is_array($config->get($cnf)) ? array_values(array_filter($config->get($cnf))) : $config->get($cnf);
      }
    }

    $response = new CacheableResponse(
      json_encode($manifest, JSON_UNESCAPED_SLASHES),
      200,
      ['Content-Type' => 'application/json']
    );
    $meta_data = $response->getCacheableMetadata();
    $meta_data->addCacheTags(['manifestjson']);
    $meta_data->addCacheContexts(['languages:language_interface']);
    return $response;

  }

  /**
   * Function to generate service worker.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   A notification for a user.
   */
  public function serviceworker($notificationTitle = "", $notificationBody = "") {

    if (empty($notificationTitle)) {
      $notificationTitle = $this->config('system.site')->get('name');
    }
    if (empty($notificationBody)) {
      $notificationBody = $this->t('You have a notification of');
    }
    $response = new CacheableResponse('', 200);
    $config = $this->config('pwa_firebase.settings');

    $firebase_version = $config->get('firebase_version', '10.12.0');

    $importScripts = [
      'importScripts("https://www.gstatic.com/firebasejs/' . $firebase_version . '/firebase-app-compat.js");',
      'importScripts("https://www.gstatic.com/firebasejs/' . $firebase_version . '/firebase-messaging-compat.js");',
      "\n",
    ];
    $firebase_messaging_sw = implode("\n", $importScripts);
    if (!empty($config->get('service_worker'))) {
      $firebase_messaging_sw .= $config->get('service_worker');
      $firebase_messaging_sw .= "\n";
    }

    // Initial Firebase app in the service worker by passing messagingSenderId.
    $project_id = trim($config->get('firebase_project_id'));
    $firebase_config = [
      'apiKey' => trim($config->get('firebase_apiKey_id')),
      'authDomain' => $project_id . '.firebaseapp.com',
      'projectId' => $project_id,
      'storageBucket' => $project_id . '.appspot.com',
      'databaseURL' => 'https://' . $project_id . '.firebaseio.com',
      'messagingSenderId' => trim($config->get('firebase_sender_id')),
      'appId' => trim($config->get('firebase_app_id')),
    ];
    if (!empty($config->get('firebase_measurement_id'))) {
      $firebase_config['measurementId'] = $config->get('firebase_measurement_id');
    }
    $firebase_messaging_sw .= 'firebase.initializeApp(' . json_encode($firebase_config, JSON_UNESCAPED_SLASHES) . ');';
    // Retrieve an instance of Firebase Messaging so that
    // it can handle background messages.
    $firebase_messaging_sw .= '
const messaging = firebase.messaging();

// This is the "Offline page" service worker
importScripts("https://storage.googleapis.com/workbox-cdn/releases/5.1.2/workbox-sw.js");

const CACHE = "pwabuilder-page";
const offlineFallbackPage = "offline.html";

self.addEventListener("message", (event) => {
  if (event.data && event.data.type === "SKIP_WAITING") {
    self.skipWaiting();
  }
});

self.addEventListener("install", async (event) => {
  event.waitUntil(
    caches.open(CACHE).then((cache) => cache.add(offlineFallbackPage))
  );
});

if (workbox.navigationPreload.isSupported()) {
  workbox.navigationPreload.enable();
}

self.addEventListener("fetch", (event) => {
  if (event.request.mode === "navigate") {
    event.respondWith((async () => {
      try {
        const preloadResp = await event.preloadResponse;
        if (preloadResp) {
          return preloadResp;
        }
        const networkResp = await fetch(event.request);
        return networkResp;
      } catch (error) {
        const cache = await caches.open(CACHE);
        const cachedResp = await cache.match(offlineFallbackPage);
        return cachedResp;
      }
    })());
  }
});
';

    $response->setContent($firebase_messaging_sw);
    // $cache_metadata = CacheableMetadata::createFromRenderArray($build);
    // $response->addCacheableDependency($cache_metadata);
    $response->headers->set('Content-type', 'application/javascript');
    $response->headers->set('Service-Worker-Allowed', '/');

    return $response;
  }

  /**
   * Function receives the user token and save's it in the database.
   *
   * @param string $token
   *   Token for each user.
   *
   * @return bool
   *   Sent or not.
   *
   * @throws \Exception
   */
  public function tokenReceived(string $token = '') {
    if (empty($token)) {
      $token = $this->request->request->get('token');
    }
    if (empty($token)) {
      return new JsonResponse(FALSE);
    }
    $hasToken = $this->database->select('pwa_firebase', 'f')
      ->fields('f')
      ->condition('token', $token, '=')
      ->execute()
      ->fetchAssoc();
    $action = $this->request->request->get('action') ?? NULL;
    if (!$hasToken) {
      $device = 'pc';
      $iPod = strpos($_SERVER['HTTP_USER_AGENT'], "iPod");
      $iPhone = strpos($_SERVER['HTTP_USER_AGENT'], "iPhone");
      $iPad = strpos($_SERVER['HTTP_USER_AGENT'], "iPad");
      $android = strpos($_SERVER['HTTP_USER_AGENT'], "Android");
      if ($iPad || $iPhone || $iPod) {
        $device = 'ios';
      }
      else {
        if ($android) {
          $device = 'android';
        }
      }

      $this->database->insert('pwa_firebase')->fields([
        'token' => $token,
        'uid' => $this->currentUser()->id(),
        'device' => $device,
        'created' => date("Y-m-d H:i:s"),
      ])->execute();
      return new JsonResponse([
        'device' => $device,
        'uid' => $this->currentUser()->id(),
      ]);
    }
    elseif (!empty($action) && $action == 'delete') {
      $this->database->delete('pwa_firebase')
        ->condition('token', $token)
        ->execute();
      return new JsonResponse(FALSE);
    }
    elseif (empty($hasToken['uid'])) {
      // Update uid with token.
      $this->database->update('pwa_firebase')
        ->fields(['uid' => $this->currentUser()->id()])
        ->condition('token', $token)
        ->execute();
    }
    return new JsonResponse(FALSE);
  }

  /**
   * Offline page for service worker.
   */
  public function offline() {
    return [
      '#markup' => $this->t('It seems you are not connected to the internet. Please check your connection and try again'),
    ];
  }

}
