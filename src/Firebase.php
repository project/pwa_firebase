<?php

namespace Drupal\pwa_firebase;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Theme\ThemeManagerInterface;
use Google\Auth\Credentials\ServiceAccountCredentials;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Promise\Utils;

/**
 * Service to send notification.
 */
class Firebase implements FirebaseInterface {
  use StringTranslationTrait;
  /**
   * Firebase configuration.
   *
   * @var mixed
   */
  protected $firebaseConfig;

  const TITLE_LENGTH = 255;
  // Assume that these are now config values.
  const MESSAGE_LENGTH = 1024;

  /**
   * Constructs a FirebaseServiceBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \GuzzleHttp\ClientInterface $client
   *   An HTTP client.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity service.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   Current patch Stack.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   Logger Factory.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   Theme manager service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   Stream wrapper manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   Language Manager service.
   */
  public function __construct(
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly ClientInterface $client,
    protected readonly Connection $connection,
    protected readonly MessengerInterface $messenger,
    protected readonly ModuleHandlerInterface $moduleHandler,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly CurrentPathStack $currentPathStack,
    protected readonly LoggerChannelFactoryInterface $loggerFactory,
    protected readonly ThemeManagerInterface $themeManager,
    protected readonly StreamWrapperManagerInterface $streamWrapperManager,
    protected readonly LanguageManagerInterface $languageManager,
  ) {
    $this->firebaseConfig = $this->configFactory->get('pwa_firebase.settings');
  }

  /**
   * Function to send a notification to all the users.
   *
   * @param string $title
   *   Message title.
   * @param string $message
   *   Message body.
   * @param mixed $url
   *   Url to article.
   * @param mixed $option
   *   Option for device type.
   */
  public function sendMessageToAllUsers($title = '', $message = '', $url = NULL, $option = []) {
    $rows = $this->getFirebaseTokens();
    return $this->prepareSend($rows, $title, $message, $url, $option);
  }

  /**
   * Function to send a notification to 1 user.
   */
  public function sendMessageToUser($uid, $title, $message, $url = NULL, $option = []) {
    $rows = $this->getFirebaseTokens(['uid' => $uid]);

    if (empty($rows)) {
      return FALSE;
    }

    $option['uid'] = $uid;
    $option['user_name'] = $this->getUserName($uid);

    return $this->prepareSend($rows, $title, $message, $url, $option);
  }

  /**
   * {@inheritDoc}
   */
  private function prepareSend(array $rows, string $title, string $message, $url = NULL, array $option = []): bool {
    $tokens = [];
    foreach ($rows as $row) {
      $token = $row->token;
      $device_type = 'pc';
      if (!empty($row->device)) {
        $device_type = $row->device;
      }
      if (!isset($tokens[$device_type])) {
        $tokens[$device_type] = [];
      }
      $tokens[$device_type][] = $token;
    }
    if (!empty($tokens)) {
      foreach ($tokens as $device => $token) {
        try {
          $option['device_type'] = $device;
          $this->sendNotification($token, $title, $message, $url, $option);
        }
        catch (\Exception $e) {
          $this->handleException($e);
        }
      }
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Function send message to firebase.
   */
  public function sendNotification($token, $title, $message, $url = NULL, $option = []) {
    global $base_url;
    $title = $this->trimText($title, self::TITLE_LENGTH);
    $message = $this->trimText($message, self::MESSAGE_LENGTH);
    $url = empty($url) ? $this->currentPathStack->getPath() : $url;
    $icon = $this->getNotificationIcon($option, $base_url, $url);
    $this->moduleHandler->alter('pwa_firebase_send', $option);
    $notification = $this->prepareNotification($title, $message, $icon, $url, $option);
    $data = $this->prepareData($notification, $token, $option);
    $fid = current($this->firebaseConfig->get('credentials_legacy'));
    if (empty($fid)) {
      $this->messenger->addError($this->t('Go to setup to upload the service account key.'));
      return [];
    }
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if (empty($file)) {
      $this->messenger->addError($this->t("Can't load the service account key."));
      return [];
    }
    $uri = $file->getFileUri();
    $stream_wrapper_manager = $this->streamWrapperManager->getViaUri($uri);
    $jsonFilePath = $stream_wrapper_manager->realpath();
    $credentials = new ServiceAccountCredentials(
      'https://www.googleapis.com/auth/firebase.messaging',
      $jsonFilePath
    );
    try {
      $accessToken = $credentials->fetchAuthToken()['access_token'];
      $headers = [
        'Content-Type' => 'application/json',
        'Accept' => 'application/json',
        'Authorization' => 'Bearer ' . $accessToken,
      ];
      return $this->postNotificationData($this->firebaseConfig->get('firebase_endpoint'), $headers, $data, $option, $token);
    }
    catch (\Exception $e) {
      $this->handleException($e);
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  private function trimText($text, $limit) {
    if (mb_strlen($text) > $limit) {
      $text = mb_substr($text, 0, $limit - 1) . '…';
    }
    return $text;
  }

  /**
   * {@inheritdoc}
   */
  private function getNotificationIcon($option, $base_url, $base_path) {
    $logo = $this->themeManager->getActiveTheme()->getLogo();
    $icon = !empty($option['image']) ? $base_url . $base_path . $option['image'] : $logo;
    if (filter_var($icon, FILTER_VALIDATE_URL) == FALSE) {
      $icon = $base_url . str_replace('//', '/', '/' . $icon);
    }
    return $icon;
  }

  /**
   * {@inheritdoc}
   */
  private function prepareNotification($title, $message, $icon, $url, &$option) {
    $notification = [
      'body' => $message,
      'title' => $title,
    ];
    foreach ($notification as $key => $value) {
      if (!empty($option[$key])) {
        $notification[$key] = $option[$key];
        unset($option[$key]);
      }
    }
    if (empty($option['icon'])) {
      $option['icon'] = $icon;
    }
    if (empty($option['link']) && !empty($url)) {
      $option['link'] = $url;
    }
    if (empty($option['lang'])) {
      $option['lang'] = $this->languageManager
        ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
        ->getId();
    }
    return $notification;
  }

  /**
   * {@inheritdoc}
   */
  private function prepareData($notification, $token, $option) {
    // Convert all values to string.
    $option = array_map('strval', $option);
    $data = [
      "notification" => $notification,
      "data" => $option,
    ];
    if (!empty($option["device_type"])) {
      switch ($option["device_type"]) {
        case 'android':
          $data['android'] = [
            "priority" => $option['priority'] ?? 'high',
            'notification' => [
              'icon' => $option['icon'],
            ],
          ];
          if (!empty($option['color'])) {
            $data['android']['notification']['color'] = $option['color'];
          }
          break;

        case 'ios':
          $data['apns'] = [
            'payload' => [
              'aps' => [
                'content_available' => !empty($option['content_available']),
                'alert' => [
                  'title' => $notification['title'],
                  'body' => $notification['body'],
                ],
              ],
            ],
          ];
          if (!empty($option['badge'])) {
            $data['apns']['payload']['aps']['badge'] = $option['badge'];
          }
          if (!empty($option['image'])) {
            $data['apns']['fcm_options']['image'] = $option['image'];
          }
          break;

        case 'pc':
          $data['webpush'] = [
            'headers' => [
              'Urgency' => 'high',
            ],
            'notification' => [
              'icon' => $option['icon'],
              'lang' => $option['lang'] ?? 'en',
            ],
            'fcm_options' => [
              'link' => $option['link'] ?? $notification['click_action'],
            ],
          ];
          if (!empty($option['badge'])) {
            $data['webpush']['notification']['badge'] = $option['badge'];
          }
          break;

      }
    }
    $messages = [];
    if (is_string($token)) {
      $token = [$token];
    }
    foreach ($token as $deviceId) {
      $data['token'] = $deviceId;
      $messages[] = $data;
    }
    return $messages;
  }

  /**
   * {@inheritdoc}
   */
  private function postNotificationData($endpoint, $headers, $data, $option, $token) {
    $result = FALSE;
    $promises = [];

    foreach ($data as $index => $message) {
      $promises[$index] = $this->client->postAsync($endpoint, [
        'headers' => $headers,
        'json' => ['message' => $message],
      ]);
    }
    $results = Utils::settle($promises)->wait();
    foreach ($results as $index => $result) {
      if ($result['state'] !== 'fulfilled') {
        $responseBody = $result['reason'];
        $this->handleNotificationFailure($responseBody, $option, $token[$index]);
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  private function handleNotificationFailure($e, $option, $token) {
    $errorMessage = $e->getMessage() . ': ' . ($option['user_name'] ?? '') . ' - ' . ($option['device'] ?? '');
    $this->loggerFactory->get('firebase')->error($errorMessage);
    $this->connection->delete('pwa_firebase')
      ->condition('token', $token)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  protected function handleException($e): void {
    $this->loggerFactory->get('firebase')->error($e->getMessage());
    $this->messenger->addError($this->t('There was an error sending the notification.'));
  }

  /**
   * {@inheritdoc}
   */
  protected function getFirebaseTokens(array $conditions = []): array {
    $tokens = [];
    try {
      $query = $this->connection->select('pwa_firebase', 'f')
        ->fields('f', ['token', 'device']);
      foreach ($conditions as $field => $value) {
        $query->condition($field, $value);
      }
      $tokens = $query->execute()->fetchAll();
    }
    catch (\Exception $e) {
      $this->handleException($e);
    }

    return $tokens;
  }

  /**
   * {@inheritdoc}
   */
  protected function getUserName(int $uid): ?string {
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    return $user ? $user->get('name')->value : NULL;
  }

}
