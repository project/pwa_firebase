# To configure Firebase in Drupal's PWA module, follow these steps
- **Setup Firebase:** Visit /admin/config/system/firebase in admin interface
- **Compose/send notification:** Access Firebase messaging interface by going to
/admin/content/notification within your Firebase console.
This allows you to create and send notifications to your PWA users.
- **Display notification permission block**: Implement a block on your website
that prompts users for notification permissions. This block should be displayed
in a prominent location to encourage users to subscribe to notifications.
- **Use an SVG logo:** It is recommended to use an SVG file for your PWA logo as
it provides scalability and responsiveness.
If you don't have an SVG logo, you can generate one by [generate logo](https://www.pwabuilder.com/imageGenerator),
After generating the logo, you will receive a file named "AppImages.zip".
AppImages.zip, Extract "AppImages.zip" contents to your theme's folder.
This step will make the logo file accessible within your Drupal theme.


### Programation:
User service **pwa_firebase.send** to send all your client by
methode send notification to all users
```
sendMessageToAllUsers($title, $message, $url);
```
methode send notification to 1 user
```
sendMessageToUser($uid, $title, $message, $url);
```
